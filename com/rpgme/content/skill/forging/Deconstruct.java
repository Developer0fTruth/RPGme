package com.rpgme.content.skill.forging;

import com.rpgme.content.event.AnvilCraftEvent;
import com.rpgme.content.event.AnvilRecipe;
import com.rpgme.content.event.AnvilRecipeListener;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.util.ItemCategory;
import com.rpgme.content.util.ItemUtil;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.LegacyConfig;
import com.rpgme.plugin.util.math.Scaler;
import net.flamedek.rpgme.Messages;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Deconstruct activates when you place a single tool/weapon/armor piece in a crafting matrix.
 * It will enable you to craft it back into raw materials.
 * The amount of materials returned is based on the current durability of the item and your Forging level.
 */
public class Deconstruct extends Ability<Forging> {

    // items that can be deconstructed
    private static final ItemCategory TARGET_ITEMS = ItemCategory.CombinedCategory.of(ItemCategory.ARMOR, ItemCategory.TOOL, ItemCategory.WEAPON);
    //private static final ItemFlag FLAG_RESULT = ItemFlag.HIDE_POTION_EFFECTS;
    private static final ItemStack RECIPE_RESULT = ItemUtil.create(Material.DIRT, (short) 1, Deconstruct.class.getSimpleName(), null);

    // percentage of ingots you get returned
    private final Scaler ingotReward = new Scaler(0,0.25, 100, 1);
    // array of materials
    private List<String> materials = Arrays.asList("leather", "stone", "iron", "gold", "chainmail", "diamond");
    // array of unlock level for above materials
    private int[] unlocks = new int[materials.size()];

    private AnvilRecipe deconstructRecipe;

    public Deconstruct(Forging skill) {
        super(skill, "Deconstruct", Forging.ABILITY_DECONSTRUCT);
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);

        for(int i = 0; i < materials.size(); i++) {
            String material = materials.get(i);
            config.addValue("unlock " + material, Math.max(1, i * 15));
        }

        //messages.addValue("format for the lore of items in crafting matrix when deconstructing", "lore_format", "Ingots: {0}\nDurability: {1}%\nSkill: {2}%\nResult: {3}");
        LegacyConfig.injectNotification(messages, getClass(), "");
        LegacyConfig.injectNotification(messages, getClass(), "_material");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        // get unlocks and put notification
        for(int i = 0; i <  materials.size(); i++) {
            String material = materials.get(i);
            String displayName = StringUtil.CamelCase(material);
            unlocks[i] = config.getInt("unlock " + material);

            String notification = LegacyConfig.getNotification(messages, getClass(), "_material", displayName);
            addNotification(unlocks[i], Notification.upgradableIcon(i+1,  materials.size()), getName() + " " + displayName, notification);
        }
        // put additional unlock notification
        int minLevel = Arrays.stream(unlocks).min().orElse(1);
        addNotification(minLevel, Notification.ICON_UNLOCK, getName(), LegacyConfig.getNotification(messages, getClass(), ""));

        initRecipe();
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < unlocks.length; i++) {
            int unlockLevel = unlocks[i];
            if(forlevel >= unlockLevel) {
                sb.append(sb.length() > 0 ? ", " : "").append(StringUtil.CamelCase(materials.get(i)));
            }
        }

        if(sb.length() > 0) {
            sb.insert(0, getName() + " types:");
            list.add(sb.toString());
            list.add(getName() + " ingot return:"+ StringUtil.readableDecimal(ingotReward.scale(forlevel)*100)+"%");
        }
    }

    private void initRecipe() {
        deconstructRecipe = new AnvilRecipe() {
            @Override
            public ItemStack getResult(InventoryView view) {
                ItemStack item = getSingleIngredient(view.getTopInventory().getContents());
                Player player = (Player) view.getPlayer();
                return getSalvageResult(player, item);
            }

            @Override
            public boolean matches(ItemStack[] ingredients) {
                ItemStack item = getSingleIngredient(ingredients);
                return !isNull(item) && TARGET_ITEMS.isItem(item);
            }

            @Override
            public void removeIngredients(InventoryView view) {
                view.setItem(0, null);
                view.setItem(1, null);
            }
        };

        AnvilRecipeListener.registerRecipe(deconstructRecipe);
    }

    private boolean isNull(ItemStack item) {
        return item == null || item.getType() == Material.AIR;
    }

    /**
     * Finds one non-null item in the first two slots of the inventory.
     * If both items are non-null, this method will return null.
     */
    private ItemStack getSingleIngredient(ItemStack[] inventory) {
        ItemStack item = null;
        for(int i = 0; i < 2; i++) {
            ItemStack invItem = inventory[i];
            if(isNull(invItem)) {
                continue;
            }
            if(isNull(item)) {
                item = invItem;
            } else {
                return null;
            }
        }
        return item;
    }

    @EventHandler
    public void onAnvilEvent(AnvilCraftEvent event) {
        if(event.getRecipe() == deconstructRecipe) {

            ItemStack deconstructedItem = getSingleIngredient(event.getInventory().getContents());
            ItemStack result = event.getCraftResult();

            float exp = getExpWorth(deconstructedItem, result);

            addExp(event.getPlayer(), exp);
        }
    }

    public float getExpWorth(ItemStack org, ItemStack result) {
        if(org == null || result == null)
            return 0f;

        String[] material = org.getType().name().split("_");
        if(material.length < 2)
            return 0f;

        RPGme.debug("amount = " + result.getAmount() + " exp = " + ExpTables.getForgingExp(material[0]));
        return (result.getAmount() * ExpTables.getForgingExp(material[0])) / 2f;
}

    public ItemStack getSalvageResult(Player player, ItemStack item) {
        if(item == null || !isEnabled(player))
            return null;

        String[] type = item.getType().name().toLowerCase().split("_");
        if(type.length < 2)
            return null;

        int level = getLevel(player);
        String material = type[0];

        if(!canSalvage(level, material)) {
            Messages.sendToActionBar(player, Messages.getMessage("err_needhigherlevel", getRequiredLevel(material), skill.getDisplayName()));
            GameSound.play(Sound.ENTITY_VILLAGER_NO, player);
            return null;
        }

        String tool = type[1];

        int ingots = Forging.getIngotsUsed(tool);
        if(ingots == 0)
            return null;

        Material returnMaterial = Forging.getRepairMaterial(material);
        if(returnMaterial == null)
            return null;

        double rewardFactor = ingotReward.scale(level);
        double durabilityFactor = 1 - (double) item.getDurability() / item.getType().getMaxDurability();
        int returnAmount = (int) Math.round(ingots * durabilityFactor * rewardFactor);

        if(returnAmount == ingots) {
            returnAmount -= 1;
        }

        if(returnAmount == 0) {
            returnMaterial = Material.STICK;
            returnAmount = 1;
        }

        return new ItemStack(returnMaterial, returnAmount);
    }

    protected boolean canSalvage(int level, String mat) {
        return level >= getRequiredLevel(mat);
    }

    protected int getRequiredLevel(String material) {
        int index = materials.indexOf(material);
        if(index < 0)
            return Integer.MAX_VALUE;
        return unlocks[index];
    }

}
