package com.rpgme.content.skill.forging.legacy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.rpgme.content.skill.Skill;
import net.flamedek.rpgme.RPGme;
import nl.flamecore.events.AnvilRecipe;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import nl.flamecore.util.ItemCategory;
import nl.flamecore.util.RomanNumber;
import nl.flamecore.util.StringUtil;

import org.apache.logging.log4j.core.config.ConfigurationException;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUpgrade extends AnvilRecipe {
	
	public static final String UPGRADES_KEY = "Upgrades";
	static final String TEXT_COLOR = ChatColor.GRAY.toString();

	final String itemCustomName;
	final int amount, max, unlocked;
	final Material material;

	final ItemCategory targets;
	final String indentifier;

	public ItemUpgrade(String indentifier,
			ItemCategory targets, String name, int amount, int max,
			int unlocked, Material material) {
		super(null, null);
		this.indentifier = indentifier;
		this.targets = targets;
		this.itemCustomName = name;
		this.amount = amount;
		this.max = max;
		this.unlocked = unlocked;
		this.material = material;
	}

	public ItemUpgrade(String indentifier, ItemCategory targets, ConfigurationSection config) throws ConfigurationException {
		super(null, null);
		this.indentifier = indentifier;

		String materialinput = config.getString("type");
		if(materialinput == null || (material = Material.matchMaterial(materialinput)) == null)
			throw new ConfigurationException("Error reading config: unknown material. '" + materialinput + "' for upgrade "+config.getName());

		this.targets = targets;

		String displayName = config.getString("name", null);
		if(displayName == null || displayName.equals("null"))
			itemCustomName = null;
		else
			itemCustomName = StringUtil.colorize(displayName.trim());

		amount = config.getInt("amount", 1);
		max = config.getInt("max", 15);
		unlocked = config.getInt("unlocked", 1);
	}

	public int getMax(Player player) {
		RPGme plugin = RPGme.plugin;
		UpgradesModule module = plugin.getModule(UpgradesModule.class);
		int playerMax = (int) module.maxUpgrades.scale(plugin.players.getLevel(player, Skill.FORGING));
		return Math.min(playerMax, max);
	}
	
	protected String getDisplay(int level) {
		return TEXT_COLOR + indentifier + ' ' + RomanNumber.toRoman(level);
	}

	@Override
	public ItemStack[] getIngredients() {
		return new ItemStack[] { new ItemStack(Material.AIR), new ItemStack(material, amount) };
	}

	@Override
	public ItemStack getResult(InventoryView view) {
		Player p = (Player) view.getPlayer();
		if(!Skill.FORGING.isEnabled(p))
			return null;

		if(unlocked > 1) {
			int level = RPGme.plugin.players.getLevel(p, Skill.FORGING);
			if(level < unlocked)
				return null;
		}
		ItemStack tool = view.getItem(0);
		ItemStack item = view.getItem(1);
		return tool == null || item == null ? null : addUpgrade(tool, getMax(p));
	}


	private ItemStack addUpgrade(ItemStack item, int max) {
		if(item == null)
			return null;

		CompoundTag itemTag = NBTFactory.getOrEmpty(item);
		CompoundTag upgradeTag = itemTag.getCompound(UPGRADES_KEY);
		if(upgradeTag == null) {
			upgradeTag = new CompoundTag();
			itemTag.putTag(UPGRADES_KEY, upgradeTag);
		}

		int upgrades = upgradeTag.getInteger(indentifier) + 1;
		if(upgrades > max)
			return null;
		upgradeTag.putInteger(indentifier, upgrades);

		ItemStack result = NBTFactory.setCompoundTag(item, itemTag);
		ItemMeta meta = result.getItemMeta();
		List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<String>();

		// remove old lore
		if(upgrades > 1) {

			Iterator<String> it = lore.iterator();

			while(it.hasNext()) {
				String line = it.next();
				if(line.startsWith(TEXT_COLOR + indentifier)) {
					it.remove();
					break;
				}
			}
		}

		// register lore
		lore.add(getDisplay(upgrades));
		meta.setLore(lore);
		result.setItemMeta(meta);

		return result;
	}

	public boolean matchItem(ItemStack item) {
		return targets.isItem(item);
	}

	public boolean matchIngredient(ItemStack item) {
		if(item == null || item.getType() != material || item.getAmount() < amount)
			return false;

		String displayName = item.getItemMeta().getDisplayName();
		if(displayName == null) {
			return itemCustomName == null;
		} else {
			return itemCustomName != null && itemCustomName.equals(displayName);
		}
	}

	@Override
	public boolean matches(ItemStack[] ingredients) {
		if(ingredients == null || ingredients.length < 2)
			return false;

		ItemStack tool = ingredients[0];
		ItemStack item = ingredients[1];

		return tool != null && item != null && matchItem(tool) && matchIngredient(item);
	}

}
