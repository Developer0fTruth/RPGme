package com.rpgme.content.skill.forging.legacy;

import com.rpgme.content.skill.forging.Forging;
import net.flamedek.rpgme.Messages;
import com.rpgme.plugin.util.exceptions.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.content.skill.Skill;
import nl.flamecore.events.AnvilRecipeListener;
import nl.flamecore.util.ItemCategory;
import nl.flamecore.util.StringUtil;

import org.apache.logging.log4j.core.config.ConfigurationException;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

class RecipeCreator {

	static final String 			
			DAMAGE = "Damage",
			SPEED = "Speed",
			SUPPORT = "Support";

	
	private BaseSkill skill;

	protected ItemUpgrade[] registerRecipes(BaseSkill skill) {
		this.skill = skill;
		ItemUpgrade[] array = new ItemUpgrade[3];
		AnvilRecipeListener.enableAnvilRecipes(skill.plugin);
		// weapon upgrades
		array[0] = add(ItemCategory.WEAPON, DAMAGE);
		array[1] = add(ItemCategory.WEAPON, SPEED);
		array[2] = add(ItemCategory.WEAPON, SUPPORT);
		
		return array;
	}
	
	private ItemUpgrade add(ItemCategory category, String name) {
		// read configuration
		ConfigurationSection section = Skill.FORGING.getConfig().getConfigurationSection(name);
		if(section == null) {
			skill.plugin.getLogger().severe("Missing configuration section: '"+name+"' under Forging. Upgrade is disabled.");
			return null;
		}
		
		try {
			// initialize upgrade
			ItemUpgrade upgrade = new ItemUpgrade(name, category, section);
			// register notification
			String material = upgrade.amount + "x " + (upgrade.itemCustomName == null ? StringUtil.reverseEnum(upgrade.material.name()) : upgrade.itemCustomName) + ChatColor.RESET;
			skill.addNotification(upgrade.unlocked, Notification.ICON_UNLOCK, "Upgrade "+ name, Messages.getNotification(Forging.class, "_upgrade", category, name, material));
			// register recipe
			AnvilRecipeListener.registerRecipe(upgrade);
			return upgrade;
		} catch(ConfigurationException e) {
			skill.plugin.getLogger().severe(e.getMessage());
			return null;
		}
		
		
	}


}
