package com.rpgme.content.skill.forging.legacy;

import java.util.List;

import com.rpgme.content.skill.forging.Forging;
import net.flamedek.rpgme.GameSound;
import net.flamedek.rpgme.Messages;
import net.flamedek.rpgme.modules.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import com.rpgme.content.nms.NMS;
import nl.flamecore.util.CoreUtil;
import nl.flamecore.util.RomanNumber;
import nl.flamecore.util.Scaler;

import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class UpgradesModule extends Ability<Forging> {

	final Scaler maxUpgrades;

	public UpgradesModule(Forging skill) {
		super(skill);

		ItemUpgrade[] recipes = new RecipeCreator().registerRecipes(skill);

		int min = 100;
		int max = 0;
		for(ItemUpgrade upgrade : recipes) {
			if(upgrade != null) {
				min = Math.min(min, upgrade.unlocked);
				max = Math.max(max, upgrade.max);
			}
		}

		maxUpgrades = new Scaler(min, 3, 100, max);

		int upgrades = 3;
		for(int i = 1; i <= 100; i++) {
			int current = (int) maxUpgrades.scale(i);
			if(current > upgrades) {
				upgrades = current;
				addNotification(i, Notification.ICON_UPGRADE, upgrades + " Upgrades", Messages.getNotification(Forging.class, "_upgrade_amount"), false);
			}
		}
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= maxUpgrades.minlvl) {
			int upgrades = (int) maxUpgrades.scale(forlevel);
			list.add("Maximum Upgrade:"+RomanNumber.toRoman(upgrades) + " ("+upgrades+')');
		}
	}

	// apply weapon offence stats
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getDamager().getType() == EntityType.PLAYER) {

			Player p = (Player) e.getDamager();
			ItemStack item = p.getInventory().getItemInMainHand();
			CompoundTag upgradesTag = getUpgradesTag(item);
			if(upgradesTag == null)
				return;

			int damageUpgrades = upgradesTag.getInteger(RecipeCreator.DAMAGE);
			int speedUpgrades = upgradesTag.getInteger(RecipeCreator.SPEED);		
			e.setDamage(e.getDamage() + (damageUpgrades * 0.25));

			if(speedUpgrades > 0 && e.getEntityType().isAlive() && CoreUtil.random.nextDouble() < speedUpgrades * 0.02) {
				new WindFuryTask(p, (LivingEntity) e.getEntity(), e.getDamage()).start(plugin);
			}
		}
	}

	// apply weapon defence stats
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntityType() == EntityType.PLAYER && e.isApplicable(DamageModifier.BLOCKING)) {
			Player p = (Player) e.getEntity();
			ItemStack item = p.getInventory().getItemInMainHand();

			CompoundTag upgradesTag = getUpgradesTag(item);
			if(upgradesTag == null)
				return;

			int ironUpgrades = upgradesTag.getInteger(RecipeCreator.SUPPORT);
			if(ironUpgrades > 0) {
				double block = e.getDamage(DamageModifier.BLOCKING) * (1 + ironUpgrades / 10.0) - ironUpgrades*0.06;
				e.setDamage(DamageModifier.BLOCKING, block);
			}
		}
	}

	private CompoundTag getUpgradesTag(ItemStack item) {
		if(item == null)
			return null;
		CompoundTag itemTag = NBTFactory.getFrom(item);
		if(itemTag == null)
			return null;

		CompoundTag upgradesTag = itemTag.getCompound(ItemUpgrade.UPGRADES_KEY);
		return upgradesTag;
	}

	private static class WindFuryTask extends BukkitRunnable {

		final Player damager;
		final LivingEntity entity;
		final double damage;

		public WindFuryTask(Player damager, LivingEntity entity, double damage) {
			this.damager = damager;
			this.entity = entity;
			this.damage = damage;
		}

		public void start(Plugin plugin) {
			runTaskLater(plugin, 7l);
		}

		@Override
		public void run() {

			if(!entity.isDead()) {
				NMS.packets.doArmSwing(damager);
				entity.damage(damage, damager);
				GameSound.play(Sound.ENTITY_ARROW_SHOOT, damager.getEyeLocation(), 0.6f, 1.5f, 0.3);
			}

		}



	}


}
