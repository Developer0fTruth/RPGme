package com.rpgme.content.skill.magic.spell;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Robin on 07/08/2016.
 */
public class SpellFilter {

    public static class Builder {

        private List<String> withName, excludeName;
        private Integer aboveTier, belowTier;
        private List<Spell.Type> withType, excludeType;
        private Boolean isCombat;
        private List<Integer> withIds, excludeIds;

        /**
         * Only match spells whose name contains one of the argument Strings
         * @param name Strings of which a spells name has to contain at least one
         * @return this for method chaining
         */
        public Builder withNameContains(String... name) {
            withName = Arrays.asList(name);
            return this;
        }

        /**
         * Filter out spells whose name contain one of the argument Strings
         * @param name Strings that will cause a spell not to match if it contains at least one.
         * @return this for method chaining
         */
        public Builder excludeNameContains(String... name) {
            excludeName = Arrays.asList(name);
            return this;
        }

        /**
         * Only match spells that are at least this tier
         * @param tier to match. Spells with exactly this tier will be included.
         * @return this for method chaining
         */
        public Builder tierAbove(int tier) {
            aboveTier = tier;
            return this;
        }

        /**
         * Only match spells that are at most this tier
         * @param tier to match. Spells with exactly this tier will be included.
         * @return this for method chaining
         */
        public Builder tierBelow(int tier) {
            belowTier = tier;
            return this;
        }

        /**
         * Filter out spells whose type does not match any of the argument types
         * @param type Types of which a spell has to be one
         * @return this for method chaining
         */
        public Builder withType(Spell.Type... type) {
            withType = Arrays.asList(type);
            return this;
        }

        /**
         * Filter out spells whose type matches any of the argument types
         * @param type Types that will cause a spell not to match.
         * @return this for method chaining
         */
        public Builder excludeType(Spell.Type... type) {
            excludeType = Arrays.asList(type);
            return this;
        }

        /**
         * Filter out spells that are marked as combat type, or not.
         * @param combat if true only match combat spells, if false do not match any combat spells
         * @return this for method chaining
         */
        public Builder isCombat(boolean combat) {
            isCombat = combat;
            return this;
        }
//
//        public Builder withId(int... ids) {
//            withIds = new ArrayList<>(ids.length);
//            for(int id : ids) {
//                withIds.add(id);
//            }
//            return this;
//        }
//
//        public Builder excludeId(int... ids) {
//            excludeIds = new ArrayList<>(ids.length);
//            for(int id : ids) {
//                excludeIds.add(id);
//            }
//            return this;
//        }

        public SpellFilter build() {
            return new SpellFilter(withName, excludeName, aboveTier, belowTier, withType, excludeType, isCombat, withIds, excludeIds);
        }
    }

    public static SpellFilter.Builder builder() {
        return new Builder();
    }

    private final List<String> withName, excludeName;
    private final Integer aboveTier, belowTier;
    private final List<Spell.Type> withType, excludeType;
    private final Boolean isCombat;
    private final List<Integer> withIds, excludeIds;

    public SpellFilter(List<String> withName, List<String> excludeName, Integer aboveTier, Integer belowTier,
                       List<Spell.Type> withType, List<Spell.Type> excludeType, Boolean isCombat,
                       List<Integer> withIds, List<Integer> excludeIds) {
        this.withName = withName;
        this.excludeName = excludeName;
        this.aboveTier = aboveTier;
        this.belowTier = belowTier;
        this.withType = withType;
        this.excludeType = excludeType;
        this.isCombat = isCombat;
        this.withIds = withIds;
        this.excludeIds = excludeIds;
    }

    public boolean matchesName(String name) {
        if(withName != null) {
            boolean match = false;
            for(String with : withName) {
                if(name.contains(with)) {
                    match = true;
                    break;
                }
            }
            if(!match)
                return false;
        }
        if(excludeName != null) {
            for(String exclude : excludeName) {
                if(name.contains(exclude)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean matchesTier(int tier) {
        if(aboveTier != null && tier < aboveTier) {
            return false;
        }
        if(belowTier != null && tier > belowTier) {
            return false;
        }
        return true;
    }

    public boolean matchesType(Spell.Type type) {
        if(withType != null) {
            boolean match = false;
            for(Spell.Type with : withType) {
                if(with.equals(type)) {
                    match = true;
                    break;
                }
            }
            if(!match)
                return false;
        }
        if(excludeType != null) {
            if(excludeType.contains(type)) {
                return false;
            }
        }
        return true;
    }

    public boolean matchesCombat(boolean combat) {
        if(isCombat != null) {
            return isCombat == combat;
        }
        return true;
    }

    public boolean matchesId(int id) {
        if(withIds != null) {
            boolean matches = false;
            for(Integer with : withIds) {
                if(with == id) {
                    matches = true;
                    break;
                }
            }
            if(!matches) {
                return false;
            }
        }
        if(excludeIds != null) {
            if(excludeIds.contains(id)) {
                return false;
            }
        }
        return true;
    }

    public boolean matches(Spell spell) {
        return //matchesId(spell.getDurability()) &&
                matchesType(spell.getType()) &&
                matchesTier(spell.getTier()) &&
                matchesCombat(spell.getType().isCombat()) &&
                matchesName(spell.getName());
    }
}
