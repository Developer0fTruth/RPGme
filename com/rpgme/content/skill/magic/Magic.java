package com.rpgme.content.skill.magic;

import com.rpgme.content.skill.SkillType;
import com.rpgme.content.skill.magic.essence.EssenceManager;
import com.rpgme.content.skill.magic.spell.Spell;
import com.rpgme.content.skill.magic.spell.SpellFilter;
import com.rpgme.content.skill.magic.spell.SpellManager;
import com.rpgme.content.skill.magic.spell.Spells;
import com.rpgme.content.skill.magic.spell.misc.TeleportHome;
import com.rpgme.content.skill.magic.staff.BaseStaff;
import com.rpgme.content.skill.magic.staff.Staff;
import com.rpgme.content.skill.magic.staff.StaffManager;
import com.rpgme.plugin.event.RPGLevelupEvent;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.BookUtil;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.Scaler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 *
 */
public class Magic extends BaseSkill {

    private Scaler magicalPower, magicalDefence, maxMana, manaRegen;

    private final SpellManager spellManager;
    private final StaffManager staffManager;
    private final ManaManager manaManager;
    private final EssenceManager essenceManager;

    public Magic() {
        super("Magic", SkillType.MAGIC);
        spellManager = new SpellManager();
        staffManager = new StaffManager();
        manaManager = new ManaManager();
        essenceManager = new EssenceManager();
    }

    @Override
    public void onEnable() {
        staffManager.registerStaff(new BaseStaff(Material.STONE_SWORD, 1, SpellFilter.builder().tierBelow(2).build()));
        spellManager.registerSpell(new TeleportHome(this));
    }

    @Override
    public Material getItemRepresentation() {
        return Material.BOOK_AND_QUILL;
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
        essenceManager.createConfig(config, bundle);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        essenceManager.onLoad(config, messages);
        int targetLevel = getPlugin().getSkillManager().getTargetLevel();
        magicalPower = new Scaler(1, 2, targetLevel, 25);
        magicalDefence = new Scaler(5, 1, targetLevel, 15);
        maxMana = new Scaler(1, 50, targetLevel, 200);
        manaRegen = new Scaler(1, 1, targetLevel, 5);
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if(forlevel >= magicalPower.minlvl) {
            list.add("Magical Power:" + magicalPower.readableScale(forlevel));
        }
        if(forlevel >= magicalDefence.minlvl) {
            list.add("Magical Defence:" + magicalPower.readableScale(forlevel));
        }
        list.add("Maximum Mana:"+(int)maxMana.scale(forlevel));
        list.add("Mana Regeneration:"+manaRegen.readableScale(forlevel) + "/s");
    }

    public SpellManager getSpellManager() {
        return spellManager;
    }

    public StaffManager getStaffManager() {
        return staffManager;
    }

    public ManaManager getManaManager() {
        return manaManager;
    }

    public int getMaxMana(Player player) {
        return (int) maxMana.scale(getLevel(player));
    }

    public double getManaRegen(Player player) {
        return manaRegen.scale(getLevel(player));
    }

    public int getMaxMana(int atLevel) {
        return (int) maxMana.scale(atLevel);
    }

    public double getManaRegen(int atLevel) {
        return manaRegen.scale(atLevel);
    }

    @EventHandler
    protected void onJoin(RPGPlayerJoinEvent event) {
        int level = event.getRPGPlayer().getLevel(getId());
        manaManager.addPlayer(event.getPlayer(), getMaxMana(level), getManaRegen(level));
    }

    @EventHandler
    protected void onLeave(PlayerQuitEvent event) {
        manaManager.removePlayer(event.getPlayer());
    }

    @EventHandler
    protected void onQuit(PlayerQuitEvent event) {
        manaManager.removePlayer(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onLevelUp(RPGLevelupEvent event) {
        if(event.getSkillId() == getId()) {
            ManaManager.ManaEntry entry = getManaManager().getEntry(event.getPlayer());
            entry.setMaxMana(getMaxMana(event.getToLevel()));
            entry.setManaRegen(getManaRegen(event.getToLevel()));
        }
    }

    @EventHandler
    public void onSpellCast(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if(event.getAction() == Action.PHYSICAL || !event.hasItem() || !isEnabled(player)) {
            return;
        }

        ItemStack item = event.getItem();
        Staff staff = staffManager.getStaff(item);
        if(staff == null) {
            return;
        }

        // temp
        item = staff.setSelectedSpell(item, Spells.TELEPORT_HOME);
        //

        RPGPlayer rpgPlayer = getPlayer(player);

        if(CoreUtil.isRightClick(event.getAction())) {
            openSpellBook(rpgPlayer, staff, item);
        }
        else {
            String spellId = staff.getSelectedSpell(item);
            Spell spell = spellManager.getSpell(spellId);

            if(spell == null) {
                openSpellBook(rpgPlayer, staff, item);
                return;
            }
            if(!manaManager.hasMana(player, spell.getManaCost())) {
                GameSound.play(Sound.ENTITY_VILLAGER_NO, player, 0.2);
                return;
            }

            if(!spell.getEssenceCost().hasItems(player)) {
                player.sendMessage(messages.getMessage("no_essence"));
            }

            try {
                boolean castSuccess = spell.castSpell(rpgPlayer, staff, item);
                if(castSuccess) {
                    manaManager.useMana(player, spell.getManaCost());
                    spell.getEssenceCost().takeItems(player);
                    // todo take items
                }
            } catch (Exception e) {
                plugin.getLogger().severe("An error occurred while casting spell " + spell.getClass().getSimpleName());
                e.printStackTrace();
            }
        }

    }

    public void openSpellBook(RPGPlayer player, Staff staff, ItemStack item) {
        List<Spell> spells = spellManager.getSpells(staff.getSpellFilter());
        Spell selectedSpell = spellManager.getSpell(staff.getSelectedSpell(item));

        ChatColor colorSelected = ChatColor.GREEN, colorUnlocked = ChatColor.WHITE, colorLocked = ChatColor.GRAY;
        int level = player.getLevel(this);

        ComponentBuilder builder = new ComponentBuilder("\n");

        for(int i = 0; i < spells.size(); i++) {
            Spell spell = spells.get(i);
            boolean selected = spell == selectedSpell;
            boolean unlocked = spell.isUnlocked(player, level);

            builder.append(" [", ComponentBuilder.FormatRetention.NONE).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, getSpellHoverText(spell, selected, unlocked))).color(selected ? colorSelected : unlocked ? colorUnlocked : colorLocked)
                    .append(String.valueOf(spell.getType().getSymbol())).color(spell.getType().getColor())
                    .append("] ").color(selected ? colorSelected : unlocked ? colorUnlocked : colorLocked);
            if(!selected && unlocked) {
                builder.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/selectspell " + spell.getName()));
            }
            if(i % 3 == 0) {
                builder.append("\n\n");
            }
        }

        builder.append("\nUse your mouse to select a spell.", ComponentBuilder.FormatRetention.NONE);
        BookUtil.openBook(player.getPlayer(), builder);
    }

    private BaseComponent[] getSpellHoverText(Spell spell, boolean isSelected, boolean isUnlocked) {
        String action = isSelected ? ChatColor.GREEN + "[SELECTED]" : isUnlocked ? ChatColor.YELLOW + "[Click to Select]" : ChatColor.DARK_GRAY + "[LOCKED]";
        String cost = spell.getEssenceCost() == null ? "None" : spell.getEssenceCost().toDisplayString();
        String description = StringUtils.join(StringUtil.splitToLength(" " + ChatColor.WHITE + ChatColor.ITALIC, spell.getDescription(), " ", 35), '\n');

        return new ComponentBuilder("  " + spell.getDisplayName()).color(spell.getType().getColor())
                .append("\n").append(description)
                .append("\n").color(ChatColor.GRAY).append("Type ").append(spell.getType().getName()).color(spell.getType().getColor())
                .append("\n").color(ChatColor.GRAY).append("Tier ").append(String.valueOf(spell.getTier())).color(ChatColor.DARK_AQUA)
                .append("\n").color(ChatColor.GRAY).append("Mana ").append(String.valueOf(spell.getManaCost())).color(ChatColor.DARK_AQUA)
                .append("\n").color(ChatColor.GRAY).append("Essence ").append(cost).color(ChatColor.DARK_AQUA)
                .append("\n  ").append(action)
                .create();
    }

}
