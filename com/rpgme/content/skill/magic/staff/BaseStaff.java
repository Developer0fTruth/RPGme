package com.rpgme.content.skill.magic.staff;

import com.rpgme.content.skill.magic.spell.SpellFilter;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by Robin on 08/08/2016.
 */
public class BaseStaff implements Staff {

    public static final String TAG_SELECTED_SPELL = "SelectedSpell";

    private final Material material;
    private final short durability;
    private final SpellFilter spellFilter;
    private Recipe[] recipes;

    public BaseStaff(Material material, int durability, SpellFilter spellFilter) {
        this.material = material;
        this.durability = (short) durability;
        this.spellFilter = spellFilter;
    }

    public void setRecipes(Recipe... recipes) {
        this.recipes = recipes;
    }

    @Override
    public SpellFilter getSpellFilter() {
        return spellFilter;
    }

    @Override
    public void addRecipes(List<Recipe> recipeList) {
        Collections.addAll(recipeList, recipes);
    }

    @Override
    public ItemStack createItem() {
        ItemStack item = new ItemStack(material, 1, durability);
        ItemMeta meta = item.getItemMeta();
        meta.spigot().setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_UNBREAKABLE);
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public boolean isItem(ItemStack item) {
        if(item == null || item.getType() != material || item.getDurability() != durability)
            return false;
        ItemMeta meta = item.getItemMeta();
        Set<ItemFlag> flags = meta.getItemFlags();
        return meta.spigot().isUnbreakable() && flags.containsAll(Arrays.asList(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_UNBREAKABLE));
    }

    @Override
    public ItemStack setSelectedSpell(ItemStack item, String selectedSpell) {
        CompoundTag tag = NBTFactory.getOrEmpty(item);
        tag.putString(TAG_SELECTED_SPELL, selectedSpell);
        return NBTFactory.setCompoundTag(item, tag);
    }

    @Override
    public String getSelectedSpell(ItemStack item) {
        CompoundTag tag = NBTFactory.getFrom(item);
        if(tag == null)
            return null;
        return tag.getString(TAG_SELECTED_SPELL);
    }
}
