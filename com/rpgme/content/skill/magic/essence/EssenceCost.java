package com.rpgme.content.skill.magic.essence;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Robin on 14-5-2016.
 */
public class EssenceCost {

    private HashMap<Essence, Integer> essenceMap = new HashMap<>(6);

    public void add(Essence key, int amount) {
        Integer previousValue = essenceMap.put(key, amount);
        if(previousValue != null) {
            essenceMap.put(key, previousValue + amount);
        }
    }

    public boolean hasItems(Player player) {
        for(Map.Entry<Essence, Integer> entry : essenceMap.entrySet()) {
            ItemStack item = entry.getKey().createItem(1);
            if(!player.getInventory().containsAtLeast(item, entry.getValue()))
                return false;
        }
        return true;
    }

    public boolean takeItems(Player player) {
        if(!hasItems(player))
            return false;
        List<ItemStack> list = new ArrayList<>(essenceMap.size());
        essenceMap.forEach((essence, amount) -> list.add(essence.createItem(amount)));
        player.getInventory().removeItem(list.toArray(new ItemStack[list.size()]));
        return true;
    }

    public String toDisplayString() {
        if(essenceMap.isEmpty()) {
            return "None";
        }
        StringBuilder builder = new StringBuilder();
        for(Map.Entry<Essence, Integer> entry : essenceMap.entrySet()) {
            if(builder.length() > 0) {
                builder.append(", ");
            }
            builder.append(entry.getValue()).append("x ").append(entry.getKey().getDisplayName());
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return "EssenceCost[" + toDisplayString() + "]";
    }

}
