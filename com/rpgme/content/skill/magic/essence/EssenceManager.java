package com.rpgme.content.skill.magic.essence;

import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Robin on 12/08/2016.
 */
public class EssenceManager implements Module {

    private List<Essence> essences = new ArrayList<>();
    private BundleSection messages;

    public EssenceManager() {

    }

    @Override
    public void onEnable() {
        essences.addAll(Arrays.asList(Essence.DUST, Essence.AIR, Essence.WATER, Essence.EARTH, Essence.FIRE, Essence.ENDER, Essence.NETHER));
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        messages.setPrefix("essence")
        .addValue("item_name", "&d{0} Essence")
        .addValue("missing", "&dYou do not have enough {0} for that spell.");

        config.beginSection("unlock level to craft each essence", "essence");
        for(int i = 0; i < essences.size(); i++) {
            Essence essence = essences.get(i);
            config.addValue(essence.getName(), Math.max(1, i * 15));
            messages.addValue(essence.getName(), essence.getName());
        }
        config.endSection();
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        this.messages = messages.getRoot().getSection("essence");
        // set configuration
        ConfigurationSection section = config.getConfigurationSection("essence");
        for(Essence essence : essences) {
            essence.setDisplayName(messages.getMessage("item_name", essence.getName()));
            essence.setUnlocked(section.getInt(essence.getName()));
        }

        // register recipes
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(new ShapedRecipe(Essence.DUST.createItem(4)).shape("ll", "ll").setIngredient('l', new ItemStack(Material.INK_SACK, 1, (short) 4).getData()));
        essences.forEach(essence -> essence.addRecipes(recipes));
        recipes.forEach(Bukkit::addRecipe);
    }

    public List<Essence> getEssences() {
        return essences;
    }

    public BundleSection getMessages() {
        return messages;
    }
}
