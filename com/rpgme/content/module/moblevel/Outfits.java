package com.rpgme.content.module.moblevel;

import com.google.common.collect.Maps;
import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

public class Outfits implements Module {

    private static final EntityOutfit EMPTY_OUTFIT = new EntityOutfit();

	private int target;
	private boolean equipArmor;
    private double difficulty;
    private float dropChance;
	private final Map<Integer, EntityOutfit> cache = Maps.newHashMap();


	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		config.addValue("Should mobs be equipped with armor, based on their level.", "equip armor", true)
                .addValue("chance of 0.0 to 1.0 if mobs drop their armor", "drop chance", 0.25);
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		equipArmor = config.getBoolean("equip armor");
        dropChance = (float) config.getDouble("drop chance");
		target = config.getInt("Target Level"); // defined in player manager
	}

    void setDifficulty(double difficulty) {
        this.difficulty = difficulty;
    }

	public EntityOutfit getArmoredOutfit(int moblevel) {
        if(!equipArmor)
            return EMPTY_OUTFIT;

		// base outfit
		boolean equipArmor = CoreUtil.random.nextDouble() < difficulty/10;
        if(!equipArmor) {
            return EMPTY_OUTFIT;
        }
		
		int tier = (int) ( (double) Math.min(target, moblevel) / target * /*tier amount*/ 14);

		EntityOutfit outfit = cache.get(tier);
		if(outfit == null) {
			outfit = build(tier).setDropChance(dropChance);
			cache.put(tier, outfit);
		}	
		return outfit;

	}

	private EntityOutfit build(int tier) {
		switch(tier) {

		case 0: return new EntityOutfit();
		case 1: return new EntityOutfit(Material.LEATHER_BOOTS, null, Material.LEATHER_CHESTPLATE);
		case 2: return new EntityOutfit(null, null, Material.LEATHER_CHESTPLATE, null, Material.WOOD_SWORD).enchant(4, Enchantment.DAMAGE_ALL, 1);

		case 3: return new EntityOutfit(null, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_CHESTPLATE)
		.enchant(2, Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		case 4: return new EntityOutfit(Material.IRON_BOOTS, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_CHESTPLATE, null, Material.STONE_SWORD);
		case 5: return new EntityOutfit(Material.CHAINMAIL_BOOTS, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_CHESTPLATE, null, null, Material.SHIELD);


		case 6: return new EntityOutfit(Material.IRON_BOOTS, Material.IRON_LEGGINGS, Material.IRON_CHESTPLATE, Material.IRON_HELMET);
		case 7: return new EntityOutfit(Material.IRON_BOOTS, Material.IRON_LEGGINGS, Material.IRON_CHESTPLATE, null, Material.IRON_SWORD)
		.enchant(4, Enchantment.LOOT_BONUS_MOBS, 2);
		case 8: return new EntityOutfit(Material.IRON_BOOTS, Material.IRON_LEGGINGS, Material.IRON_CHESTPLATE, null, Material.IRON_SWORD)
		.enchant(2, Enchantment.PROTECTION_ENVIRONMENTAL, 2).enchant(1, Enchantment.PROTECTION_ENVIRONMENTAL, 1);

		case 9: return new EntityOutfit(Material.DIAMOND_BOOTS, Material.IRON_LEGGINGS, Material.IRON_CHESTPLATE, null, Material.DIAMOND_SWORD);
		case 10 : return new EntityOutfit(Material.IRON_BOOTS, Material.DIAMOND_LEGGINGS,Material.IRON_CHESTPLATE, null, null, Material.SHIELD);
		case 11 : return new EntityOutfit(Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, null, Material.DIAMOND_HELMET, Material.STONE_SWORD, Material.SHIELD);

		case 12 : return new EntityOutfit(Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, null, Material.DIAMOND_HELMET, Material.IRON_SWORD).enchant(4, Enchantment.DAMAGE_ALL, 1);
		case 13: return new EntityOutfit(Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_HELMET, Material.DIAMOND_SWORD)
		.enchant(4, Enchantment.DAMAGE_ALL, 1).enchant(2, Enchantment.PROTECTION_ENVIRONMENTAL, 2).enchant(0, Enchantment.PROTECTION_ENVIRONMENTAL, 1);

		default: return new EntityOutfit();
		}
	}

	public static class EntityOutfit {

		private final ItemStack[] content;
		private double speed = -1;
		private int health = -1;

        private float dropChance;

		public EntityOutfit(Material... armor) {
			if(armor.length == 0)
				content = null;
			else {
				content = new ItemStack[6];

				ItemStack air = new ItemStack(Material.AIR);
				for(int i = 0; i < 6; i++) {
					if(i < armor.length) {
						content[i] = armor[i] != null ? new ItemStack(armor[i]) : air;
					} else content[i] = air;
				}
			}
		}

        public EntityOutfit setDropChance(float dropChance) {
            this.dropChance = dropChance;
            return this;
        }

        public EntityOutfit enchant(int slot, Enchantment ench, int level) {
			ItemStack item = content[slot];
			if(item != null && ench != null)
				item.addUnsafeEnchantment(ench, level);
			return this;
		}

		public EntityOutfit setSpeed(double speed) {
			this.speed = speed;
			return this;
		}

		public EntityOutfit setHealth(double health) {
			this.health = (int) Math.round(health);
			return this;
		}

		public EntityOutfit equip(LivingEntity entity) {

			if(content != null) {
				EntityEquipment equip = entity.getEquipment();
				equip.setArmorContents(content);

				equip.setBootsDropChance(dropChance);
				equip.setLeggingsDropChance(dropChance);
				equip.setChestplateDropChance(dropChance);
				equip.setHelmetDropChance(dropChance);
				equip.setItemInMainHandDropChance(dropChance);
				equip.setItemInOffHandDropChance(dropChance);
				
			}
			if(speed > 0)
				NMS.util.setMovementSpeed(entity, speed);
			if(health > 0) {
				entity.setMaxHealth(Math.ceil(health));
				entity.setHealth(health);
			}
			return this;
		}

		public EntityOutfit withWeapon(LivingEntity entity, ItemStack weapon) {
			entity.getEquipment().setItemInMainHand(weapon);
			return this;
		}
		public EntityOutfit withOffhand(LivingEntity entity, ItemStack weapon) {
			entity.getEquipment().setItemInOffHand(weapon);
			return this;
		}

	}

}
