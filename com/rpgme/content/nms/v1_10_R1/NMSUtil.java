package com.rpgme.content.nms.v1_10_R1;

import com.rpgme.content.nms.INMS.INMSUtil;
import net.minecraft.server.v1_10_R1.Entity;
import net.minecraft.server.v1_10_R1.EntityArrow;
import net.minecraft.server.v1_10_R1.EntityInsentient;
import net.minecraft.server.v1_10_R1.GenericAttributes;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class NMSUtil implements INMSUtil {
	
	public Player getNearestPlayer(Location loc) {
		double distance = -1;
		Player player = null;
		
		for(Player p : loc.getWorld().getPlayers()) {
			
			double dis = loc.distanceSquared(p.getEyeLocation());
			if(distance < 0 || distance > dis) {
				player = p;
				distance = dis;
			}
		}
		return player;
	}

	@Deprecated
	public Player getNearestPlayer(Location loc, double range) {
		return getNearestPlayer(loc);
	}
	
	@Deprecated
	public Player getNearestPlayer(World world, double x, double y, double z, double range) {
		return getNearestPlayer(new Location(world, x, y, z));
	}

	public void setAbsorptionHearts(LivingEntity p, float extra) {
		((CraftLivingEntity)p).getHandle().setAbsorptionHearts(extra);
	}
	
	public float getAbsorptionHearts(LivingEntity p) {
		return ((CraftLivingEntity)p).getHandle().getAbsorptionHearts();
	}

	public void setInvisible(org.bukkit.entity.Entity entity, boolean value) {
		((CraftEntity)entity).getHandle().setInvisible(value);
	}
	
	public void setMovementSpeed(org.bukkit.entity.Entity entity, double value) {
		Entity nms = ((CraftEntity)entity).getHandle();
		((EntityInsentient)nms).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(value);
		
	}
	
	public boolean isFromMobspawner(org.bukkit.entity.Entity entity) {
		return ((CraftEntity)entity).getHandle().fromMobSpawner;
	}
	
	public boolean canPickup(Arrow arrow) {
		return ((EntityArrow)((CraftEntity)arrow).getHandle()).fromPlayer == EntityArrow.PickupStatus.ALLOWED;
	}
	
	public void setCanPickup(Arrow arrow, boolean value) {
		((EntityArrow)((CraftEntity)arrow).getHandle()).fromPlayer = EntityArrow.PickupStatus.ALLOWED;
	}


}
