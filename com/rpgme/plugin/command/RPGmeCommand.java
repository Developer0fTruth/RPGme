package com.rpgme.plugin.command;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.StringUtil;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class RPGmeCommand extends CoreCommand {
	
	private static final String USEAGE_SET = "/rpg set <player> <skill> <level>",
			USEAGE_SETALL = "/rpg setall <player> <level>",
			USEAGE_ADDEXP = "/rpg addexp <player> <skill> <exp>";

	public RPGmeCommand(RPGme plugin) {
		super(plugin, "rpgme", "rpgme.setexp", "rpgme.save", "rpgme.reload");
		setConsoleAllowed(true);
		setShowHelp(false);
		
		setAliases("rpg");
		setDescription("Some admin features: 'set' 'addexp' 'setall' and 'reload'");
		setUsage(USEAGE_SET + '\n' + USEAGE_ADDEXP + '\n' + USEAGE_SETALL);
		setExample("/rpg set notch archery 100\n/rpg addexp steve attack 1000\n/rpg setall herobrine 1");
	}

	@Override
	public void execute(CommandSender sender, String alias, List<String> flags) {

		if(flags.isEmpty() || flags.contains("?") || flags.contains("help")) {

			plugin.getCommandManager().printCommandHelp(this, sender);
			
		}else if(flags.remove("set")) {

			handleSetExp(sender, flags, false);

		} else if(flags.remove("addexp")) {

			handleSetExp(sender, flags, true);
			
		} else if(flags.remove("setall")) {
			
			handleSetAllLevels(sender, flags);
			
		} else if(flags.remove("reload")) {
			
			if(!hasPermission(sender, "rpgme.reload")) {
				return;
			}
			
			plugin.reloadConfig();
			sender.sendMessage(StringUtil.colorize("&aRPGme (&e"+plugin.getDescription().getVersion()+"&a) - Files reloaded."));
			
		} else {
			plugin.getCommandManager().printCommandHelp(this, sender);
		}
		
	}
	
	
	private void handleSetAllLevels(CommandSender sender, List<String> flags) {
		if(!hasPermission(sender, "rpgme.command.setexp")) {
			return;
		}

		if(flags.size() < 2) {
			error(sender, USEAGE_SETALL);
			return;
		}
		
		Player target = plugin.getServer().getPlayer(flags.get(0));

		if(target == null) {
			error(sender, "Error first argument: Targer player '"+flags.get(0)+"' not found.");
			return;
		}
		
		int newvalue;

		try {
			newvalue = ExpTables.xpForLevel(Integer.parseInt(flags.get(1))) + 1;
		} catch(NumberFormatException e) {
			error(sender, "Error third argument: '"+flags.get(1)+"' is not a number.");
			return;
		}
		
		RPGPlayer rp = plugin.getPlayer(target);
		for(int skill : plugin.getSkillManager().getKeys()) {
			rp.setExp(skill, newvalue);
		}
		rp.getSkillSet().recalculateTotalLevel();
		rp.updateScoreboard();
		
		sender.sendMessage(ChatColor.YELLOW + " All levels of "+target.getName() + " set to " + flags.get(1));

	}
	
	private void handleSetExp(CommandSender sender, List<String> flags, boolean add) {

		if(!hasPermission(sender, "rpgme.command.setexp")) {
			return;
		}

		if(flags.size() < 3) {
			error(sender, "Not enough arguments. Use /rpg help");
			return;
		}

		Player target = plugin.getServer().getPlayer(flags.get(0));

		if(target == null) {
			error(sender, "Error first argument: Target player '"+flags.get(0)+"' not found.");
			return;
		}

		Skill skill = plugin.getSkillManager().getByName(flags.get(1));

		if(skill == null) {
			error(sender, "Error second argument: Skill '"+flags.get(1)+"' not recognized.");
			return;
		} else if(!skill.isEnabled(target)) {
			error(sender, target.getName() + " does not have permission to use that skill.");
			return;
		}

		int amount;

		try {
			amount = Integer.parseInt(flags.get(2));
		} catch(NumberFormatException e) {
			error(sender, "Error third argument: '"+flags.get(2)+"' is not a number.");
			return;
		}

		RPGPlayer rp = plugin.getPlayer(target);
		
		if(add) {
			rp.addExp(skill, amount);
			sender.sendMessage(ChatColor.YELLOW.toString() + amount + " " + skill.getDisplayName() + " exp added to player "+target.getName());
		}
		else {
			amount = ExpTables.xpForLevel(amount) + 1;
			rp.setExp(skill.getId(), amount);
			sender.sendMessage(ChatColor.YELLOW + skill.getDisplayName() + " level set to " + amount + " for player "+target.getName());
		}
		
		rp.getSkillSet().recalculateTotalLevel();
		rp.updateScoreboard();

	}
//
//	@Override
//	public void addCommandHelp(CommandSender sender, List<String> list) {
//		if(hasPermission(sender, "rpgme.command.setexp", false)) {
//			list.add(USEAGE_SET + ":Set a players level for a specified skill.");
//			list.add(USEAGE_SETALL + ":Set all skills of a player to a level.");
//			list.add(USEAGE_ADDEXP + ":Add exp for a skill to a player.");
//		}
//		if(hasPermission(sender, "rpgme.command.reload", false)) {
//			list.add("/rpg reload:Reload manager files. Note that most config values are already cached, and will not update before a restart.");
//		}
//	}


    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        return null;
    }
}
