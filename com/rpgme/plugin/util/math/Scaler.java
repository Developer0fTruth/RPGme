package com.rpgme.plugin.util.math;

import com.rpgme.plugin.util.CoreUtil;
import com.rpgme.plugin.util.StringUtil;

public class Scaler {

	public final double minvalue, maxvalue;
	public final int minlvl, maxlvl;

	public Scaler(int minlvl, double minvalue, int maxlvl, double maxvalue) {
		this.minlvl = minlvl;
		this.minvalue = minvalue;
		this.maxlvl = maxlvl;
		this.maxvalue = maxvalue;
	}
	
	public String readableScale(double level) {
		return StringUtil.readableDecimal(scale(level));
	}
	
	public double scale(double level) {
		if(level <= minlvl)
			return minvalue;
		
		double perStep = (maxvalue - minvalue) / (maxlvl - minlvl);
		double steps = level - minlvl;
		return perStep < 0 ? Math.max(maxvalue, minvalue + (perStep*steps)) : Math.min(maxvalue, minvalue + (perStep * steps));
	}

	/**
	 * If value represents a percentage chance, roll a boolean
	 */
	public boolean isRandomChance(double level) {
		return CoreUtil.random.nextDouble() < scale(level)/100;
		
	}
	
	
	
}
