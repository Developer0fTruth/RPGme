package com.rpgme.plugin.util;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.effect.ParticleEffect;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class TreasureBag implements Module {

    /** Convenience method to getEntry the instance registered with the plugin */
    public static TreasureBag getInstance() {
        return RPGme.getInstance().getModule(RPGme.MODULE_TREASURES);
    }

	protected final RPGme plugin;
	protected final List<TreasureStack> treasure;

	public TreasureBag(RPGme plugin) {
		this.plugin = plugin;
		treasure = new ArrayList<>();
        initLoot();
	}

	public TreasureBag(RPGme plugin, List<TreasureStack> treasures) {
        if(treasures == null) {
            treasures = Collections.emptyList();
        }
	    this.plugin = plugin;
		this.treasure = treasures;
	}
	
	protected void initLoot() {
		// vanilla loot
		addTreasure(Material.SADDLE, 1);
		addTreasure(Material.NAME_TAG, 50);

		addTreasure(new ItemStack(Material.SKULL_ITEM, 1, (short) 1), 75);

		addTreasure(new ItemStack(Material.ENDER_PEARL, 6), 20);
		addTreasure(new ItemStack(Material.ENDER_PEARL, 16), 60);
		addTreasure(new ItemStack(Material.EXP_BOTTLE, 16), 25);
		addTreasure(new ItemStack(Material.EXP_BOTTLE, 32), 50);

		addTreasure(Material.IRON_BARDING, 1);
		addTreasure(Material.GOLD_BARDING, 40);
		addTreasure(Material.DIAMOND_BARDING, 60);

		addTreasure("musicdisc", 1);

		addTreasure(new ItemStack(Material.ARROW, 32), 1);
		addTreasure(new ItemStack(Material.ARROW, 64), 30);

		addTreasure(Material.CHAINMAIL_BOOTS, 40);
		addTreasure(Material.CHAINMAIL_CHESTPLATE, 60);
		addTreasure(Material.CHAINMAIL_HELMET, 50);
		addTreasure(Material.CHAINMAIL_LEGGINGS, 50);

		addTreasure(Material.DIAMOND_BOOTS, 70);
		addTreasure(Material.DIAMOND_CHESTPLATE, 70);
		addTreasure(Material.DIAMOND_HELMET, 70);
		addTreasure(Material.DIAMOND_LEGGINGS, 70);
		addTreasure(Material.DIAMOND_SWORD, 30);

		addTreasure(new ItemStack(Material.GHAST_TEAR, 2), 10);
		addTreasure(new ItemStack(Material.GHAST_TEAR, 4), 30);

		// exp tombs
		addTreasure("exptomb500", 1);
		addTreasure("exptomb1000,10", 30);
		addTreasure("exptomb2500,25", 40);
		addTreasure("exptomb5000,50", 90);
		
		addTreasure("skillgem", 95);
		
//		if(RPGme.getInstance().getConfig().getBoolean("Skill Enchants.enabled")) {
//			addTreasure("skillenchantment10", 1);
//			addTreasure("skillenchantment25", 45);
//			addTreasure("skillenchantment40", 85);
//		}
	}

	
	public void addTreasure(ItemStack item, int rarity) {
		treasure.add(new TreasureStack(item, rarity));
	}

	public void addTreasure(Material item, int rarity) {
		addTreasure(new ItemStack(item), rarity);
	}
	
	public void addTreasure(String item, int rarity) {
		Map<String, Object> map = new HashMap<String, Object>(2);
		map.put("rarity", rarity);
		map.put("type", item);
		treasure.add(TreasureStack.deserialize(map));
	}

	public List<TreasureStack> getTreasures() {
		return treasure;
	}

	public ItemStack rollTreasure(int level) {
		List<Integer> factors = new ArrayList<Integer>();
		int total = 0;

		for(TreasureStack stack : treasure) {
			int f = stack.getWeight(level);

			factors.add(f);
			total += f;

		}

		int index = CoreUtil.random.nextInt(total);
		int sum = 0;
		int i = 0;

		while(sum < index) {
			sum += factors.get(i++);
		}

		return treasure.get(Math.max(0, i-1)).getItemStack();
	}

	public void spawnTreasure(Block b, int level) {
		ItemStack item = rollTreasure(level);
		spawnTreasure(b, item);
	}

	public void spawnTreasure(Location loc, int level) {
		ItemStack item = rollTreasure(level);
		spawnTreasure(loc, item);
	}
	public void spawnTreasure(Block b, ItemStack item) {
		spawnTreasure(b.getLocation().add(0.5,0.5,0.5), item);
	}

	public void spawnTreasure(Location loc, ItemStack item) {
		ParticleEffect.HEART.display(0.2f, 0.2f, 0.2f, 1f, 2, loc, 16);
		ParticleEffect.FIREWORKS_SPARK.display(0.25f, 0.25f, 0.25f, 0.2f, 5, loc, 16);
		GameSound.play(Sound.BLOCK_NOTE_HARP, loc, 2f, 1.4f);

		loc.getWorld().dropItemNaturally(loc, item);
	}

}
