package com.rpgme.plugin.util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;

import java.util.EnumSet;
import java.util.Set;

public class EntityTypes {

	public static final Set<EntityType> hostileEntities = EnumSet.of(EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CREEPER, EntityType.ENDERMAN,
			EntityType.GHAST, EntityType.MAGMA_CUBE, EntityType.PIG_ZOMBIE, EntityType.SKELETON, EntityType.SILVERFISH, 
			EntityType.SLIME, EntityType.SPIDER, EntityType.WITCH, EntityType.ZOMBIE, EntityType.GUARDIAN, EntityType.ENDERMITE);
	
	public static final Set<EntityType> passiveEntities = EnumSet.of(EntityType.BAT, EntityType.CHICKEN, EntityType.COW, EntityType.HORSE, 
			EntityType.IRON_GOLEM, EntityType.MUSHROOM_COW, EntityType.OCELOT, EntityType.PIG, EntityType.RABBIT, EntityType.SHEEP, 
			EntityType.SQUID, EntityType.VILLAGER, EntityType.WOLF);
	
	public static final Set<EntityType> netherEntities = EnumSet.of(EntityType.BLAZE, EntityType.MAGMA_CUBE, EntityType.PIG_ZOMBIE);
	
	public static final Set<EntityType> bossEntities = EnumSet.of(EntityType.WITHER, EntityType.ENDER_DRAGON);
	
	public static boolean isHostile(Entity e) {
		return hostileEntities.contains(e.getType());
	}
	public static boolean isPassive(Entity e) {
		return passiveEntities.contains(e.getType());
	}
	
	public static boolean isNetherMob(Entity e) {
		EntityType type = e.getType();
		if(type == EntityType.SKELETON)
			return ((Skeleton)e).getSkeletonType() == SkeletonType.WITHER;
		else
			return netherEntities.contains(type);
		
	}
	public static boolean isBoss(Entity e) {
		EntityType type = e.getType();
		if(type == EntityType.GUARDIAN)
			return ((Guardian)e).isElder();
		else
			return bossEntities.contains(type);
	}

}
