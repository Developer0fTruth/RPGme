package com.rpgme.plugin.util.nbtlib;

//@formatter:off

/*
 * JNBT License
 * 
 * Copyright (c) 2010 Graham Edgecombe
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *       
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       
 *     * Neither the name of the JNBT team nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */

//@formatter:on

import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;

/**
 * The <code>TAG_List</code> tag.
 * 
 * @author Graham Edgecombe
 * 
 */
public final class ListTag extends Tag implements Iterable<Tag> {
		
	/**
	 * The value.
	 */
	private final List<Tag> value;
	
	/**
	 * The type of values
	 */
	private byte type = 0;
	
	/**
	 * Creates the tag.
	 * 
	 * @param name
	 *            The name.
	 */            
	public ListTag(final String name) {
		super(name);
		this.value = Lists.newArrayList();
	}
	
	public boolean add(Tag arg) {
		if(type == 0)
			type = arg.getTypeId();
		else if(type != arg.getTypeId())
			 System.err.println("WARNING: Adding mismatching tag types to tag list");
		
		return value.add(arg);
	}
	
	public void add(int index, Tag arg) {
		if(type == 0)
			type = arg.getTypeId();
		else if(type != arg.getTypeId())
			 System.err.println("WARNING: Adding mismatching tag types to tag list");
		
		value.add(index, arg);
	}

	public boolean contains(Object arg0) {
		return value.contains(arg0);
	}

	public boolean remove(Object arg0) {
		return value.remove(arg0);
	}

	
	@Override
	public List<Tag> getValue() {
		return value;
	}
	
	@Override
	public byte getTypeId() {
		return NBTConstants.TYPE_LIST;
	}
	
	public byte getValueType() {
		return type;
	}
	
	@Override
	public String toString() {
	
		final String name = getName();
		String append = "";
		if ((name != null) && !name.equals("")) {
			append = "(\"" + getName() + "\")";
		}
		final StringBuilder bldr = new StringBuilder();
		bldr.append("TAG_List" + append + ": " + value.size()
				+ "\r\n{\r\n");
		for (final Tag t : value) {
			bldr.append("   " + t.toString().replaceAll("\r\n", "\r\n   ")
					+ "\r\n");
		}
		bldr.append("}");
		return bldr.toString();
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
	
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
	
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (!(obj instanceof ListTag)) { return false; }
		final ListTag other = (ListTag) obj;
		if (value == null) {
			if (other.value != null) { return false; }
		} else if (!value.equals(other.value)) { return false; }
		return true;
	}

	@Override
	public Iterator<Tag> iterator() {
		return getValue().iterator();
	}
	
}
