package com.rpgme.plugin.skill;

import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.StringUtil;
import com.rpgme.plugin.util.Symbol;

import org.apache.commons.lang3.StringUtils;

import static com.rpgme.plugin.util.Symbol.STAR_FULL;

/**
 *
 */
public class Notification implements Comparable<Notification> {

	public static class Builder {

		int level = LVL_NONE;
		String icon, title, text;

		boolean sticky = true;
		RPGPlayer owner;

		public Notification build() {
			return new Notification(level, icon, title, text, sticky, owner);
		}

		public Builder level(int level) {
			this.level = level;
			return this;
		}

		public Builder icon(String icon) {
			this.icon = icon;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder text(String message) {
			this.text = message;
			return this;
		}

		public Builder sticky(boolean sticky) {
			this.sticky = sticky;
			return this;
		}

		public Builder player(RPGPlayer owner) {
			this.owner = owner;
			return this;
		}
	}

	public static Notification.Builder builder() {
		return new Builder();
	}

	public static final String
			ICON_UNLOCK = "✪",
			ICON_UPGRADE = String.valueOf(STAR_FULL),
			ICON_PASSIVE = "✉";

	public static String upgradableIcon(int step, int max) {
        return StringUtils.repeat(STAR_FULL, step) + StringUtils.repeat(Symbol.STAR_EMPTY, max - step);
    }

	public static final int LVL_NONE = Integer.MAX_VALUE;

	private final int level;
    private final String icon, title, text;
    private final boolean sticky;

	private RPGPlayer owner = null;

	public Notification(int level, String icon, String title, String text, boolean sticky, RPGPlayer owner) {
		this.owner = owner;
		this.sticky = sticky;
		this.text = StringUtil.colorize(text);
		this.title = title;
		this.icon = icon;
		this.level = level;
	}

	/**
	 * Construct a one-time notification for a specific player
	 */
	public Notification(RPGPlayer player, String title, String text) {
		this(LVL_NONE, ICON_UNLOCK, title, text, false, player);
	}


	public Notification(int level, String icon, String title, String text) {
        this(level, icon, title, text, true, null);
    }

	public Notification(int level, String icon, String title, String text, boolean sticky) {
       this(level, icon, title, text, sticky, null);
    }

	public int getLevel() {
        return Math.max(1, level);
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public String getText() {
		return text == null ? "" : text;
    }

	public boolean isSticky() {
        return sticky;
    }

	public String getIcon() {
		return icon == null ? "" : icon;
	}

	public RPGPlayer getOwner() {
		return owner;
	}

	@Override
    public int compareTo(Notification arg0) {
        return Integer.compare(level == LVL_NONE ? -1 : level, arg0.getLevel());
    }

}
