package com.rpgme.plugin.integration.plugin;

import com.palmergames.bukkit.towny.db.TownyDataSource;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyPermission.ActionType;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.palmergames.bukkit.towny.object.WorldCoord;
import com.palmergames.bukkit.towny.utils.CombatUtil;
import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.rpgme.plugin.integration.PluginIntegration.TeamPlugin;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Towny implements BlockProtectionPlugin, TeamPlugin {
	
	@Override
	public void enable(Plugin plugin) {
	
	}

	@Override
	public PlayerRelation getRelation(Player one, Player two) {
		TownyDataSource data = TownyUniverse.getDataSource();
		
		if(!data.hasTown(one.getName()) || !data.hasTown(two.getName()))
			return PlayerRelation.NEUTRAL;
		
		try {
			
			Town townOne = data.getResident(one.getName()).getTown();
			Town townTwo = data.getResident(two.getName()).getTown();
			
			if(townOne == townTwo || CombatUtil.isAlly(townOne, townTwo))
				return PlayerRelation.TEAM;
			
			else if(CombatUtil.isEnemy(townOne, townOne))
				return PlayerRelation.ENEMIES;
			
			else return PlayerRelation.NEUTRAL;
		} catch (NotRegisteredException e) {
			return PlayerRelation.NEUTRAL;
		}
	}

	@Override
	public boolean canChange(Player player, Block block) {
		try {
			Town town = WorldCoord.parseWorldCoord(block).getTownBlock().getTown();
			if(town.hasResident(player.getName()))
				return town.getPermissions().getResidentPerm(ActionType.BUILD);
			
			return town.getPermissions().getOutsiderPerm(ActionType.BUILD);
		} catch (NotRegisteredException e) {

			return true;
		}
	}
	
	@Override
	public boolean isInClaim(Block block) {
		try {
			WorldCoord.parseWorldCoord(block).getTownBlock().getTown();
			return true;
		} catch (NotRegisteredException e) {
			return false;
		}
		
	}

	@Override
	public String getPluginName() {
		return "Towny";
	}

}
