package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class WorldGuard implements BlockProtectionPlugin {
	
	private WorldGuardPlugin wg;
	
	public WorldGuard() {
		
	}
	
	@Override
	public void enable(Plugin plugin) {
		wg = (WorldGuardPlugin) plugin;
	}

	@Override
	public boolean canChange(Player p, Block block) {
		return wg.canBuild(p, block);
	}
	
	@Override
	public boolean isInClaim(Block block) {
		return wg.getRegionManager(block.getWorld()).getApplicableRegions(block.getLocation()).size() > 0;
		
	}

	@Override
	public String getPluginName() {
		return "WorldGuard";
	}

	

}
